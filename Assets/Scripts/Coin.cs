﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Coin : WorldColoredObject
{
  private SpriteRenderer spriteRenderer;
  private BoxCollider2D boxCollider;

  private void Awake()
  {
    spriteRenderer = GetComponent<SpriteRenderer>();
    boxCollider = GetComponent<BoxCollider2D>();
  }

  protected override void HandleStateActivated()
  {
    boxCollider.enabled = true;
    spriteRenderer.DOFade(1f, 0.25f);
  }

  protected override void HandleStateDeactivated()
  {
    boxCollider.enabled = false;
    spriteRenderer.DOFade(GameManager.Instance.currentEmotionState == EmotionState.Neutral ? 0.25f : 0.0f, 0.25f);
  }

  private void OnTriggerEnter2D(Collider2D other)
  {
    if(other.CompareTag("Player"))
    {
      GetGot();
    }
  }

  private void GetGot()
  {
    // Play some kind of animation, and then deactivate
    spriteRenderer.DOFade(0, 0.5f).onComplete = () => gameObject.SetActive(false);
  }
}
