﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
  public static MenuManager Instance { get; private set; }
  public ConstrainedInput constrainedInput;
  public static bool paused { get { return Instance && Time.timeScale == 0; } private set { } }

  [Tooltip("Is there already a menu in the scene that should be added?")]
  [SerializeField] private Menu startMenu;
  private Stack<Menu> menuStack = new Stack<Menu>();

  // Menus we can load using a type (Similar to GetComponent. Static because we should only need to fill this once)
  private static Dictionary<System.Type, Menu> menuChoices;

  private void Awake()
  {
    if (Instance == null)
    {
      Instance = this;
    }
    else
    {
      Destroy(this);
    }
  }

  private void OnDestroy()
  {
    if (Instance == this)
      Instance = null;
  }

  // Use this for initialization
  void Start()
  {
    // Load menus if our dictionary is empty
    if (menuChoices == null)
      LoadMenus();

    if (startMenu)
    {
      menuStack.Push(startMenu);
      startMenu.Open();
      constrainedInput.SetMenu(startMenu);
      startMenu.isFocused = true;
    }
  }

  // See if we can load all our menus from the resources folder
  // (we only want to do this once)
  private void LoadMenus()
  {
    Debug.Log("MenuManager Loading Menus");
    menuChoices = new Dictionary<System.Type, Menu>();
    Object[] menusToLoad = Resources.LoadAll("Menus");
    for (int i = 0; i < menusToLoad.Length; i++)
    {
      GameObject menuObj = menusToLoad[i] as GameObject;
      menuChoices.Add(menuObj.GetComponent<Menu>().GetType(), menuObj.GetComponent<Menu>());
    }
  }

  // Open a menu of type T and return it so it can be messed with
  public T LoadMenu<T>() where T : Menu
  {
    Menu menu = Instantiate(menuChoices[typeof(T)], transform);
    // Update sorting layer automatically
    menu.GetComponent<Canvas>().sortingOrder = menuStack.Count > 0 ?
        menuStack.Peek().GetComponent<Canvas>().sortingOrder + 1 : 0;

    // Hide all lower menus if applicable
    if (menuStack.Count > 0 && menu.hideMenusUnderneath)
    {
      foreach (Menu underMenu in menuStack)
      {
        underMenu.Hide();
        // Stop hiding if we reach another menu that disables lower ones
        if (underMenu.hideMenusUnderneath)
          break;
      }
    }

    if (menuStack.Count > 0)
    {
      menuStack.Peek().GetComponent<GraphicRaycaster>().enabled = false;
      menuStack.Peek().isFocused = false;
    }

    constrainedInput.SetMenu(menu);
    menuStack.Push(menu);
    menu.isFocused = true;
    return menu as T;
  }

  public void CloseMenu()
  {
    Destroy(menuStack.Pop().gameObject);
    if (menuStack.Count > 0)
    {
      menuStack.Peek().Show();
      menuStack.Peek().GetComponent<GraphicRaycaster>().enabled = true;
      menuStack.Peek().isFocused = true;
      constrainedInput.SetMenu(menuStack.Peek());
    }
  }

  private void Update()
  {
    if (Input.GetKeyDown(KeyCode.Escape) && menuStack.Count > 0)
    {
      menuStack.Peek().OnBackPressed();
    }
  }
}