﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ConstrainedInput : StandaloneInputModule
{
  // Must be children of this to be selectable
  private Menu currentMenu;

  protected override void Awake()
  {
    base.Awake();
    if (!currentMenu)
      currentMenu = GetComponent<Menu>();
  }

  public override void Process()
  {
    GameObject obj = eventSystem.currentSelectedGameObject;
    base.Process();
    if (eventSystem.currentSelectedGameObject)
    {
      //Debug.Log(eventSystem.currentSelectedGameObject.name);
      if (!eventSystem.currentSelectedGameObject.transform.IsChildOf(currentMenu.transform))
      {
        eventSystem.SetSelectedGameObject(obj);
      }
    }
  }

  public void SetMenu(Menu menu)
  {
    if (currentMenu && currentMenu.trackLastSelected) { currentMenu.lastSelected = eventSystem.currentSelectedGameObject; }
    currentMenu = menu;
    eventSystem.SetSelectedGameObject(currentMenu.lastSelected ? currentMenu.lastSelected : currentMenu.firstSelected);
  }

}