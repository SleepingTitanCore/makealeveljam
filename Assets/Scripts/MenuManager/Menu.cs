﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Menu : MonoBehaviour
{
  [Tooltip("What gameobject should be focused when this menu is opened for the first time")]
  public GameObject firstSelected;

  [Tooltip("Disable the menus below this one")]
  public bool hideMenusUnderneath = false;

  [Tooltip("Whether or not to keep track of the last selected GameObject in the menu")]
  public bool trackLastSelected = true;

  // Whether this menu is the one that's active
  public bool isFocused;

  // Last-selected gameObject 
  [System.NonSerialized] public GameObject lastSelected;

  public virtual void OnBackPressed() { Close(); }
  protected virtual void OnDestroy() { }
  public virtual void Open() { Show(); }
  public virtual void Close() { MenuManager.Instance.CloseMenu(); }
  public virtual void Hide() { gameObject.SetActive(false); }
  public virtual void Show() { gameObject.SetActive(true); }
}