﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class WorldColoredObject : MonoBehaviour
{
  public EmotionState targetEmotionState;

  protected virtual void OnEnable()
  {
    GameManager.EmotionStateChanged += HandleEmotionStateChange;
  }

  protected virtual void OnDisable()
  {
    GameManager.EmotionStateChanged -= HandleEmotionStateChange;
  }

  private void HandleEmotionStateChange(EmotionStateInfo obj)
  {
    if (obj.currentState == targetEmotionState)
      HandleStateActivated();
    else
      HandleStateDeactivated();
  }

  protected abstract void HandleStateActivated();

  protected abstract void HandleStateDeactivated();

}
