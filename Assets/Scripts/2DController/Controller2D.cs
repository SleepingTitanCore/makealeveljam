﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;

public class Controller2D : MonoBehaviour
{
  [System.Serializable]
  public class ControllerSettings
  {
    public float gravity;
    public float jumpHeight = 2.2f;
    public float minJumpHeight = 0.1f;
    public float moveSpeed = 5f;
    public float runAcceleration = 20;
    public float stopAcceleration = 40;
    public float airAcceleration = 15f;
    public float airDeceleration = 15f;
    public float wallJumpForce = 5f;
    public bool canWallJump = true;
  }

  [Range(1, 5)]
  public uint collisionCheckIterations = 3;
  public float maxVelocityY = 10.0f;
  public float requiredSadBreakVelocity = 2.5f;
  public ControllerSettings Neutral, Happy, Sad, Angry;
  public bool isGrounded { get; private set; }
  public Vector3 desiredVelocity { get { return _desiredVelocity; } private set { _desiredVelocity = value; } }
  public Vector3 actualVelocity { get { return _actualVelocity; } private set { _actualVelocity = value; } }
  public bool isWallSliding { get; private set; }
  public ParticleSystem ps;

  private ControllerSettings currentSettings;
  private Vector3 _desiredVelocity;
  private Vector3 _actualVelocity;
  private BoxCollider2D _collider;
  private ControllerAnimator2D _animController;
  private bool jumpButtonUsed = false;
  private float currentJumpHeight;
  private Vector2 wallNormal;

  private void Awake()
  {
    _collider = GetComponent<BoxCollider2D>();
    _animController = GetComponent<ControllerAnimator2D>();
    SetControllerSettings(Neutral);
  }

  private void OnEnable() {
    GameManager.EmotionStateChanged += HandleEmotionStateChange;
  }

  private void OnDisable() {
    GameManager.EmotionStateChanged -= HandleEmotionStateChange;
  }

  // Update is called once per frame
  private void Update()
  {
    desiredVelocity = actualVelocity;
    Vector3 temp = new Vector3(transform.position.x, transform.position.y, transform.position.z);
    if (isGrounded)
    {
      _desiredVelocity.y = 0;
      if (Input.GetKeyDown(KeyCode.Space))
      {
        _desiredVelocity.y = Mathf.Sqrt(2 * currentSettings.jumpHeight * Mathf.Abs(Physics2D.gravity.y));
        jumpButtonUsed = true;
        currentJumpHeight = 0;
      }
    }
    else if(isWallSliding)
    {
      if (Input.GetKeyDown(KeyCode.Space))
      {
        Debug.Log("WallJumpinnnnn");
        _desiredVelocity.y = Mathf.Sqrt(2 * currentSettings.jumpHeight * Mathf.Abs(Physics2D.gravity.y));
        _desiredVelocity.x = wallNormal.x * currentSettings.wallJumpForce;
        jumpButtonUsed = true;
        currentJumpHeight = 0;
      }
    }

    if(!isGrounded && jumpButtonUsed && !Input.GetKey(KeyCode.Space) && _actualVelocity.y > 0)
    {
      _desiredVelocity.y = Mathf.Sqrt(2 * currentSettings.minJumpHeight * Mathf.Abs(Physics2D.gravity.y));
      jumpButtonUsed = false;
    }

    _desiredVelocity.y += Physics2D.gravity.y * Time.deltaTime;

    if (_desiredVelocity.y < -maxVelocityY) _desiredVelocity.y = -maxVelocityY;
    if (_desiredVelocity.y > maxVelocityY) _desiredVelocity.y = maxVelocityY;


    // Update Horizontal Movement
    float moveInput = Input.GetAxisRaw("Horizontal");
    float acceleration = isGrounded ? currentSettings.runAcceleration : currentSettings.airAcceleration;
    float deceleration = isGrounded ? currentSettings.stopAcceleration : currentSettings.airDeceleration;

    if (moveInput == 0 || (isGrounded && ((moveInput < 0 && _desiredVelocity.x > 0) || (moveInput > 0 && _desiredVelocity.x < 0)))) // If we're stopped or trying to move in the opposite direction
    {
      _desiredVelocity.x = Mathf.MoveTowards(_desiredVelocity.x, 0, deceleration * Time.deltaTime);
    }
    else // If we're running like normal
    {
      _desiredVelocity.x = Mathf.MoveTowards(_desiredVelocity.x, moveInput * currentSettings.moveSpeed, acceleration * Time.deltaTime);
    }
    transform.Translate(_desiredVelocity * Time.deltaTime);

    bool wasGrounded = isGrounded;
    bool hitGround = false;

    // Solve collisions
    isGrounded = false;
    isWallSliding = false;
    Collider2D[] hits = Physics2D.OverlapBoxAll(transform.position + new Vector3(_collider.offset.x * transform.localScale.x, _collider.offset.y * transform.localScale.y, 0), 
      new Vector2(_collider.size.x * transform.localScale.x, _collider.size.y * transform.localScale.y), 0);

    for (int i = 0; i < collisionCheckIterations; i++)
    {
      foreach (Collider2D collider in hits)
      {

        if (collider == _collider || collider.isTrigger) // Skip our own collider
          continue;

        ColliderDistance2D dist = collider.Distance(_collider);
        if (dist.isOverlapped)
        {
          transform.Translate(dist.pointA - dist.pointB);
          if (Vector2.Angle(dist.normal, Vector2.up) < 45 && _desiredVelocity.y < 0)
          {
            isGrounded = true;
            jumpButtonUsed = false;
            if(!wasGrounded)
            {
              hitGround = true;
            }
          }
          if(Vector2.Angle(dist.normal, Vector2.up) > 85 && !isGrounded && currentSettings.canWallJump)
          {
            // Debug.Log(Vector2.Angle(dist.normal, Vector2.up));
            wallNormal = dist.normal;
            isWallSliding = true;
          }
        }
      }
    }

    _actualVelocity = (transform.position - temp) / Time.deltaTime;

    if (hitGround)
    {
      HandleLanding();
    }

    if (_animController)
      _animController.UpdateValues();
  }


  private void HandleEmotionStateChange(EmotionStateInfo emotionStateInfo)
  {
    switch(emotionStateInfo.currentState)
    {
      case EmotionState.Neutral:
        SetControllerSettings(Neutral);
        break;

      case EmotionState.Happy:
        SetControllerSettings(Happy);
        break;

      case EmotionState.Sad:
        SetControllerSettings(Sad);
        break;

      case EmotionState.Angry:
        SetControllerSettings(Angry);
        break;
    }
  }

  private void SetControllerSettings(ControllerSettings settings)
  {
    Physics2D.gravity = new Vector2(Physics2D.gravity.x, settings.gravity);
    currentSettings = settings; 
  }

  private void HandleLanding()
  {
    ps.Play();
    if(GameManager.Instance.currentEmotionState == EmotionState.Sad && Mathf.Abs(desiredVelocity.y) >= requiredSadBreakVelocity)
    {
      Camera.main.DOShakePosition(0.25f, 0.05f, 30, 70, true).onComplete = () => Camera.main.transform.DOLocalMove(Vector3.zero, 0.2f);
      GameManager.Instance.tilemap.SetTile(new Vector3Int((int)transform.position.x, (int)(transform.position.y - 1f), 0), null);
    }
  }

}
