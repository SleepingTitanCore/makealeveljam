﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerAnimator2D : MonoBehaviour
{
  private Controller2D _controller;
  private Animator _animator;
  private SpriteRenderer _spriteRenderer;

  // Start is called before the first frame update
  void Awake()
  {
    _controller = GetComponent<Controller2D>();
    _animator = GetComponent<Animator>();
    _spriteRenderer = GetComponent<SpriteRenderer>();
  }

  private void OnEnable()
  {
    GameManager.EmotionStateChanged += TriggerAnimState;
  }

  private void OnDisable()
  {
    GameManager.EmotionStateChanged -= TriggerAnimState;
  }

  private void TriggerAnimState(EmotionStateInfo obj)
  {
    _animator.SetTrigger(obj.currentState.ToString());
  }

  public void UpdateValues()
  {
    _animator.SetBool("IsGrounded", _controller.isGrounded);
    _animator.SetFloat("VelocityX", Mathf.Abs(_controller.actualVelocity.x));
    _animator.SetFloat("VelocityY", _controller.desiredVelocity.y);
    _animator.SetBool("WallSliding", _controller.isWallSliding && !_controller.isGrounded);

    if (_controller.desiredVelocity.x < 0)
    {
      if(_controller.isWallSliding && !_controller.isGrounded)
      {
        _spriteRenderer.flipX = false;
      }
      else
      {
        _spriteRenderer.flipX = true;
      }
    }
    else if (_controller.desiredVelocity.x > 0)
    {
      if (_controller.isWallSliding && !_controller.isGrounded)
      {
        _spriteRenderer.flipX = true;
      }
      else
      {
        _spriteRenderer.flipX = false;
      }
    }
  }
}
