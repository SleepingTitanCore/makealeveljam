﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCamera : MonoBehaviour
{
  public LayerMask raymask;
  public float baseFOV = 60f;
  public float baseRotationSpeed = 5f;
  public float zoomedRotationSpeed = 2.5f;
  public float zoomedFOV = 45;
  public float zoomSpeed = 8f;
  public float maxTilt = 165f;
  public float minTilt = 10f;
  public float cameraDistance;
  public Vector3 lookOffset;
  public bool useOffset = true;
  public Transform target;
  public float raycastRadius = 0.25f;

  private Vector3 cameraDir = -Vector3.forward;
  private Vector3 targetPos;
  private Camera cam;
  private Vector3 lookAtPos;
  private Transform tempTransform; // Used so that we aren't taking rotation into account
  private float rotationSpeed;

  // Start is called before the first frame update
  void Start()
  {
    Cursor.lockState = CursorLockMode.Locked;
    cam = GetComponent<Camera>();
    cam.transform.position = new Vector3(target.position.x, target.position.y + lookOffset.y, target.position.z - cameraDistance);
    tempTransform = new GameObject().GetComponent<Transform>();
  }

  // Update is called once per frame
  void LateUpdate()
  {
    UpdateRoam();
  }

  // Update our 3rd-person follow camera
  private void UpdateRoam()
  {
    tempTransform.position = target.position;
    tempTransform.forward = new Vector3(transform.forward.x, 0, transform.forward.z).normalized;
    if (Input.GetMouseButton(1))
    {
      cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, zoomedFOV, Time.deltaTime * zoomSpeed);
      rotationSpeed = zoomedRotationSpeed;
    }
    else
    {
      cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, baseFOV, Time.deltaTime * zoomSpeed);
      rotationSpeed = baseRotationSpeed;
    }


    Vector2 angleChange = new Vector2(Input.GetAxis("Mouse X") * rotationSpeed, -Input.GetAxis("Mouse Y") * rotationSpeed);

    float currentAngle = -Vector3.SignedAngle(transform.forward, Vector3.up, transform.right);

    if (currentAngle + angleChange.y > maxTilt)
      angleChange.y -= (currentAngle + angleChange.y) - maxTilt;
    else if (currentAngle + angleChange.y < minTilt)
      angleChange.y += minTilt - (currentAngle + angleChange.y);

    // Rotate our camera vector on the x axis first, then the Y.
    cameraDir = Quaternion.AngleAxis(angleChange.y, transform.right) * cameraDir;
    cameraDir = Quaternion.AngleAxis(angleChange.x, Vector3.up) * cameraDir;

    // Update our target position for our camera by raycasting for obstacles first.
    lookAtPos = tempTransform.TransformPoint(lookOffset);
    Ray ray = new Ray(lookAtPos, cameraDir);
    RaycastHit hit;

    if (Physics.SphereCast(ray, raycastRadius, out hit, cameraDistance, raymask))
      targetPos = hit.point + hit.normal * raycastRadius;
    else
      targetPos = lookAtPos + cameraDir * cameraDistance;

    // Begin lerping our position
    transform.position = targetPos;

    // Finally, look at our target.
    transform.LookAt(lookAtPos);
  }

}
