﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public enum EmotionState
{
  Neutral,
  Sad,
  Happy,
  Angry
}

public class EmotionStateInfo
{
  public EmotionState currentState;
  public EmotionState previousState;
}

public class GameManager : MonoBehaviour
{
  public static GameManager Instance;
  public Tilemap tilemap;
  public static event Action<EmotionStateInfo> EmotionStateChanged;
  public Color NeutralColor, HappyColor, SadColor, AngryColor;
  public Controller2D playerController;
  public float stateTransitionTime;
  public Camera mainCamera { get; set; }

  public EmotionState currentEmotionState { get; private set; }
  private bool inited = false;

  // Start is called before the first frame update
  void Awake()
  {
    if (Instance == null) {
      Instance = this;
    }
    else {
      Destroy(this.gameObject);
      return;
    }

    mainCamera = Camera.main;
  }


  private void Start()
  {
    SetEmotionState(EmotionState.Neutral);
  }

  private void Update()
  {
    if(Input.GetKeyDown(KeyCode.Q)) {
      SetEmotionState(EmotionState.Neutral);
    }
    if (Input.GetKeyDown(KeyCode.E)) {
      SetEmotionState(EmotionState.Happy);
    }
    if (Input.GetKeyDown(KeyCode.R)) {
      SetEmotionState(EmotionState.Sad);
    }
    if (Input.GetKeyDown(KeyCode.F)) {
      SetEmotionState(EmotionState.Angry);
    }
  }

  public void SetEmotionState(EmotionState state)
  {
    if (currentEmotionState == state && inited) return; // We only want to invoke state change events when our state actually changes.
    currentEmotionState = state;
    EmotionStateChanged?.Invoke(new EmotionStateInfo { currentState = state, previousState = currentEmotionState });
    inited = true;
  }

  public Color GetCurrentEmotionColor()
  {
    switch(currentEmotionState)
    {
      case EmotionState.Neutral:
        return NeutralColor;

      case EmotionState.Happy:
        return HappyColor;

      case EmotionState.Sad:
        return SadColor;

      case EmotionState.Angry:
        return AngryColor;

      default:
        return NeutralColor;
    }
  }
}
