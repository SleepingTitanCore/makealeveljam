﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

// Super basic interactable class 
[RequireComponent(typeof(Rigidbody))]
public class Interactable : MonoBehaviour
{

  [Tooltip("Position character must run to in order to interact")]
  public Transform interactPosition;

  [Tooltip("Whether or not the player can activate or use this.")]
  public bool playerCanUse = true;

  [Tooltip("Whether or not to emit special glow")]
  public bool emitGlow = true;

  public bool isFocused { get; private set; }

  // Iteraction events that can be triggered by anything that wants to interact
  public UnityEvent onInteract;

  // When the player focuses on this interactable. What happens?
  public UnityEvent onFocusStart;
  public UnityEvent onFocusEnd;

  private static Material hoverMat;
  private GameObject itemParticleSystem;

  private Renderer[] interactableRenderers;

  public virtual void Interact()
  {
    onInteract.Invoke();
  }

  protected virtual void Awake()
  {
    if (!hoverMat)
    {
      hoverMat = Resources.Load("OutlineMat") as Material;
      hoverMat.color = Color.clear;
    }

    interactableRenderers = GetComponentsInChildren<Renderer>();
    onFocusStart.AddListener(() => { AddFocusMat(); isFocused = true; });
    onFocusEnd.AddListener(() => { RemoveFocusMat(); isFocused = false; });
  }

  private void AddFocusMat()
  {
    if (!hoverMat) return;
    foreach (Renderer renderer in interactableRenderers)
    {
      List<Material> materials = new List<Material>(renderer.materials);
      materials.Add(hoverMat);
      renderer.materials = materials.ToArray();
    }
  }

  public void SetGlowEnabled(bool enabled)
  {
    if (enabled)
    {
      if (!itemParticleSystem) { CreateItemParticleSystem(); }
      else { itemParticleSystem.SetActive(true); }
    }
    else
    {
      itemParticleSystem.SetActive(false);
    }
  }

  public void Deactivate()
  {
    SetGlowEnabled(false);
    playerCanUse = false;
  }

  private void CreateItemParticleSystem()
  {
    GameObject particleSystem = Resources.Load("ItemParticleSystem") as GameObject;
    itemParticleSystem = Instantiate(particleSystem, transform, false);
    itemParticleSystem.transform.localPosition = Vector3.zero;
  }

  private void RemoveFocusMat()
  {
    if (!hoverMat) return;
    foreach (Renderer renderer in interactableRenderers)
    {

      List<Material> materials = new List<Material>(renderer.materials);

      for (int i = 0; i < materials.Count; i++)
      {
        if (materials[i].name.Contains(hoverMat.name))
        {
          materials.RemoveAt(i);
        }
      }

      renderer.materials = materials.ToArray();
    }

  }
}