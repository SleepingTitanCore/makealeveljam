﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public class InteractableSensor : MonoBehaviour
{
  // The interactable we're currently focused on.
  public Interactable focusedInteractable { get; private set; }

  // List of interactables that we're in range of
  public List<Interactable> interactablesInRange = new List<Interactable>();

  // Update is called once per frame
  void Update()
  {
    Interactable currentInteractable = focusedInteractable;

    GetRidOfNullAndInactiveShiz(); // Running this during every update stanks, but OnTriggerExit doesn't get called when an object is destroyed

    // Don't search if there's nothing in range
    if (interactablesInRange.Count == 0)
    {
      focusedInteractable = null;
    }
    else if (interactablesInRange.Count == 1)
    {
      focusedInteractable = interactablesInRange[0];
    }
    else
    {
      float minDistance = -1;
      Interactable closest = null;
      foreach (Interactable interactable in interactablesInRange)
      {
        if (interactable == null) { continue; }
        float distance = Vector3.Distance(transform.position, interactable.transform.position);
        if (distance < minDistance || minDistance < 0)
        {
          minDistance = distance;
          closest = interactable;
        }
      }
      focusedInteractable = closest;
    }

    if (currentInteractable != focusedInteractable)
    {
      if (currentInteractable)
      {
        currentInteractable.onFocusEnd.Invoke();
      }
      if (focusedInteractable)
      {
        focusedInteractable.onFocusStart.Invoke();
      }
    }
  }

  private void OnTriggerEnter(Collider other)
  {
    Interactable iactable = other.GetComponent<Interactable>();
    if (iactable && iactable.playerCanUse)
    {
      interactablesInRange.Add(iactable);
    }
  }

  // Other interactables might need to have rigidbodies for onTriggerExit to work correctly....
  private void OnTriggerExit(Collider other)
  {
    Interactable interactable = other.GetComponent<Interactable>();
    if (interactable)
    {
      interactablesInRange.Remove(interactable);
    }
  }

  private void GetRidOfNullAndInactiveShiz()
  {
    for (int i = 0; i < interactablesInRange.Count; i++)
    {
      if (interactablesInRange[i] == null || !interactablesInRange[i].playerCanUse ||
          !interactablesInRange[i].gameObject.activeSelf)
      {
        interactablesInRange.RemoveAt(i);
      }
    }
  }
}