﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using System;

// Example utility class that's meant to be the "visual" transition which triggers the asynch scene loading behavior.
public class SceneTransitioner : MonoBehaviour
{
  public Image transitionImage;
  public Image transitionAnimImage;
  public CanvasGroup canvasGroup;
  public float animTime = 0.5f;

  public void Start()
  {
    SceneLoader.Instance.OnLoadUpdate += UpdateLoad;
    SceneLoader.Instance.OnLoadComplete += LoadComplete;
  }

  private void Update()
  {
    transitionAnimImage.rectTransform.Rotate(0, 0, 30f * Time.deltaTime);
  }

  private void LoadComplete()
  {
    canvasGroup.DOFade(0, animTime);
  }

  private void UpdateLoad(float obj)
  {
    Debug.Log("Loading progress: " + obj);
  }

  public void LoadScene(string scene)
  {
    // First animate in, then start loading scene.
    canvasGroup.DOFade(1, animTime).onComplete = () => SceneLoader.Instance.LoadSceneAsyncronous(scene);
  }

  public void LoadSceneRegular(string scene)
  {
    canvasGroup.DOFade(1, animTime).onComplete = () => SceneLoader.Instance.LoadSceneRegular(scene);
  }
}
