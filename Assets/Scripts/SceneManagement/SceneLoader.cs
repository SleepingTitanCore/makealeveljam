﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// This class is meant for being able to transition between scenes asynchronously 
// so that we might use loading bars instead of freezing the entire game.
public class SceneLoader : MonoBehaviour
{
  public static SceneLoader Instance;

  public event Action<float> OnLoadUpdate;
  public event Action OnLoadComplete;

  private AsyncOperation asyncOperation;

  private void Awake()
  {
    if (Instance == null)
    {
      Instance = this;
      DontDestroyOnLoad(this.gameObject);
    }
    else
    {
      Destroy(this);
      return;
    }
  }

  private void Update()
  {
    if (Input.GetKeyDown(KeyCode.Escape)) Application.Quit();
  }

  public void LoadSceneAsyncronous(string scene)
  {
    StopAllCoroutines();
    StartCoroutine(LoadSceneAsync(scene));
  }

  public void LoadSceneRegular(string scene)
  {
    SceneManager.LoadScene(scene);
  }

  IEnumerator LoadSceneAsync(string name)
  {
    asyncOperation = SceneManager.LoadSceneAsync(name, LoadSceneMode.Single);

    while(!asyncOperation.isDone)
    {
      OnLoadUpdate?.Invoke(asyncOperation.progress);
      yield return null;
    }
    OnLoadComplete?.Invoke();
  }
}
