﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ColorChanger : MonoBehaviour
{
  private SpriteRenderer spriteRenderer;

  private void Awake()
  {
    spriteRenderer = GetComponent<SpriteRenderer>();
  }

  private void OnEnable()
  {
    GameManager.EmotionStateChanged += HandleEmotionStateChange;
  }

  private void OnDisable()
  {
    GameManager.EmotionStateChanged -= HandleEmotionStateChange;
  }

  private void HandleEmotionStateChange(EmotionStateInfo obj)
  {
    spriteRenderer.DOColor(GameManager.Instance.GetCurrentEmotionColor(), GameManager.Instance.stateTransitionTime);
  }
}
