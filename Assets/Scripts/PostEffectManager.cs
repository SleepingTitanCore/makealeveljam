﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using DG.Tweening;
using UnityEngine.Tilemaps;

public class PostEffectManager : MonoBehaviour
{
  [System.Serializable]
  public class EmotionEffectSettings
  {
    public float saturation;
    public float brightness;
    public float bloomIntensity;
  }

  public PostProcessVolume postProcessVolume;
  public float chromaticPunch = 1;
  public float brightnessPunch = 1;
  public float bloomPunch;
  public EmotionEffectSettings neutralSettings;
  public EmotionEffectSettings happySettings;
  public EmotionEffectSettings sadSettings;
  public EmotionEffectSettings angrySettings;
  public Tilemap tilemap;

  private ColorGrading colorGrade;
  private Bloom bloom;
  private ChromaticAberration chromaticAberration;

  private void Awake()
  {
    colorGrade = postProcessVolume.profile.GetSetting<ColorGrading>();
    chromaticAberration = postProcessVolume.profile.GetSetting<ChromaticAberration>();
    bloom = postProcessVolume.profile.GetSetting<Bloom>();
    colorGrade.enabled.value = true;
    colorGrade.colorFilter.value = Color.white;
    colorGrade.saturation.value = 0;
    colorGrade.brightness.value = 0;
  }

  private void OnEnable() {
    GameManager.EmotionStateChanged += HandleStateChange;
  }

  private void OnDisable() {
    GameManager.EmotionStateChanged -= HandleStateChange;
  }

  private void HandleStateChange(EmotionStateInfo emotionStateInfo)
  {
    switch (emotionStateInfo.currentState)
    {
      case EmotionState.Neutral:
        SetSettings(neutralSettings);
        break;

      case EmotionState.Happy:
        SetSettings(happySettings);
        break;

      case EmotionState.Sad:
        SetSettings(sadSettings);
        break;

      case EmotionState.Angry:
        SetSettings(angrySettings);
        break;
    }
  }

  private void SetSettings(EmotionEffectSettings settings)
  {
    DoSaturation(settings.saturation, GameManager.Instance.stateTransitionTime);
    PunchBrightness(settings.brightness, brightnessPunch, GameManager.Instance.stateTransitionTime);
    PunchBloom(settings.bloomIntensity, bloomPunch, GameManager.Instance.stateTransitionTime);
    PunchChromatic(0, chromaticPunch, GameManager.Instance.stateTransitionTime);
    DOTween.To(() => tilemap.color, x => tilemap.color = x, GameManager.Instance.GetCurrentEmotionColor(), GameManager.Instance.stateTransitionTime);
  }

  private Tween DoSaturation(float finalSaturationValue, float duration) {
    return DOTween.To(() => colorGrade.saturation.value, x => colorGrade.saturation.value = x, finalSaturationValue, duration);
  }

  private Tween DoColorFilter(Color finalColor, float duration) {
    return DOTween.To(() => colorGrade.colorFilter.value, x => colorGrade.colorFilter.value = x, finalColor, duration);
  }

  private Tween DoBrightness(float finalBrightnessValue, float duration) {
    return DOTween.To(() => colorGrade.brightness.value, x => colorGrade.brightness.value = x, finalBrightnessValue, duration);
  }

  private Tween PunchBrightness(float finalBrightnessValue, float direction, float duration) {
    Sequence seq = DOTween.Sequence();
    seq.Append(DoBrightness(finalBrightnessValue + direction, duration / 2.0f));
    seq.Append(DoBrightness(finalBrightnessValue, duration / 2.0f));
    return seq;
  }

  private Tween DoBloomIntensity(float finalIntensity, float duration) {
    return DOTween.To(() => bloom.intensity.value, x => bloom.intensity.value = x, finalIntensity, duration);
  }

  private Tween PunchBloom(float finalIntensity, float direction, float duration) {
    Sequence seq = DOTween.Sequence();
    seq.Append(DoBloomIntensity(finalIntensity + direction, duration / 2.0f));
    seq.Append(DoBloomIntensity(finalIntensity, duration / 2.0f));
    return seq;
  }

  private Tween DoChromatic(float finalIntensity, float duration)
  {
    return DOTween.To(() => chromaticAberration.intensity.value, x => chromaticAberration.intensity.value = x, finalIntensity, duration);
  }

  private Tween PunchChromatic(float finalIntensity, float direction, float duration)
  {
    Sequence seq = DOTween.Sequence();
    seq.Append(DoChromatic(finalIntensity + direction, duration / 2.0f));
    seq.Append(DoChromatic(finalIntensity, duration / 2.0f));
    return seq;
  }
}
