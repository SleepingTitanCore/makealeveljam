﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

// Class that "plays" a dialog sequence and calls events when finished.
public class DialogPlayer : MonoBehaviour
{
  [Tooltip("The visible text that will be \"Played\". It is typically in a dialog box.")]
  public TextMeshProUGUI targetUIText;

  [Tooltip("The number of characters that will be revealed per second.")]
  public float charactersPerSec = 20;

  [Tooltip("Whether or not someone can skip a statement animation by pressing E")]
  public bool canSkip;

  public event Action StatementStarted; // Called when the next statement in a sequence starts playing.
  public event Action StatementFinished; // Called when a statement has been finished.
  public event Action LastStatementFinished; // Called when the last statement of this sequence is finished playing.
  public event Action SequenceStarted; // Called when a sequence is started.
  public event Action SequenceFinished; // Called when we finish our current sequence.
  public bool isPlaying { get; private set; }
  public DialogSequence currentSequence { get; private set; }

  private bool advanceTriggered;

  public void Advance()
  {
    advanceTriggered = true;
  }

  public void PlayDialogSequence(DialogSequence sequence)
  {
    // Don't do anything if our sequence is empty
    currentSequence = sequence;
    if (sequence == null || sequence.statements.Count == 0) return;

    StopAllCoroutines();
    StartCoroutine(PlayDialog(sequence));
  }

  private IEnumerator PlayDialog(DialogSequence sequence)
  {
    isPlaying = true;
    SequenceStarted?.Invoke();
    int statementIndex = 0;
    float numCharactersToAdd = 0;

    while (statementIndex < sequence.statements.Count)
    {
      targetUIText.text = sequence.statements[statementIndex].statement;
      targetUIText.maxVisibleCharacters = 0;
      StatementStarted?.Invoke();
      yield return null;

      while (targetUIText.maxVisibleCharacters < sequence.statements[statementIndex].statement.Length)
      {
        // If we press one of the continue keys, finish out this sentence.
        if (canSkip && advanceTriggered)
        {
          targetUIText.maxVisibleCharacters = sequence.statements[statementIndex].statement.Length;
          advanceTriggered = false;
          break;
        }

        numCharactersToAdd += charactersPerSec * Time.deltaTime;
        if ((int)numCharactersToAdd > 0)
        {
          targetUIText.maxVisibleCharacters += (int)numCharactersToAdd;
          numCharactersToAdd -= (int)numCharactersToAdd;
        }
        yield return null;
      }

      // Here, check if we've reached the end of our statements. If so, populate our actions if necessary
      yield return null;
      StatementFinished?.Invoke();
      if (statementIndex >= sequence.statements.Count - 1)
      {
        LastStatementFinished?.Invoke();
        while (!advanceTriggered)
        {
          yield return null;
        }
        advanceTriggered = false;
        StopPlaying();
        statementIndex++;
      }
      else
      {
        while (!advanceTriggered)
        {
          yield return null;
        }
        advanceTriggered = false;
        statementIndex++;
      }
    }
  }

  private void StopPlaying()
  {
    isPlaying = false;
    SequenceFinished?.Invoke();
  }
}
