﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using System;
using UnityEngine.Events;

[RequireComponent(typeof(CanvasGroup))]
public class DialogBox : MonoBehaviour
{
  // We'll only ever have one dialog box, so let's make it a singleton for easier use.
  public static DialogBox Instance;

  public DialogPlayer dialogPlayer;
  public Image continueSprite;
  public GameObject responseButtonPrefab;
  public HorizontalLayoutGroup dialogActionGroup;
  public float openAnimationDuration = 0.25f;
  public DialogSequence debugSequence;
  public UnityEvent OnDialogFinish;

  private CanvasGroup canvasGroup;
  private List<Button> responseButtons = new List<Button>();
  private RectTransform rectTransform;
  private bool showingActions;

  private void Awake()
  {
    if (Instance == null)
      Instance = this;
    else
      Destroy(this);

    canvasGroup = GetComponent<CanvasGroup>();
    rectTransform = GetComponent<RectTransform>();

    for (int i = 0; i < 4; i++)
    {
      Button responseButton = Instantiate(responseButtonPrefab, dialogActionGroup.transform, false).GetComponent<Button>();
      responseButtons.Add(responseButton);
    }
  }

  private void Start()
  {
    Close(true);
    SetButtonsActive(false);
    if(debugSequence != null)
    {
      PlayDialogSequence(debugSequence);
    }
  }

  private void OnEnable()
  {
    dialogPlayer.SequenceStarted += SequenceStartedHandler;
    dialogPlayer.SequenceFinished += SequenceFinishedHandler;
    dialogPlayer.StatementStarted += StatementStartedHandler;
    dialogPlayer.StatementFinished += StatementFinishedHandler;
    dialogPlayer.LastStatementFinished += LastStatementHandler;
  }

  private void OnDisable()
  {
    dialogPlayer.SequenceStarted -= SequenceStartedHandler;
    dialogPlayer.SequenceFinished -= SequenceFinishedHandler;
    dialogPlayer.StatementStarted -= StatementStartedHandler;
    dialogPlayer.StatementFinished -= StatementFinishedHandler;
    dialogPlayer.LastStatementFinished -= LastStatementHandler;
  }

  private void Update()
  {
    if(Input.GetKeyDown(KeyCode.E) && !showingActions)
    {
      dialogPlayer.Advance();
    }
  }

  private void StatementFinishedHandler()
  {
    continueSprite.enabled = true;
  }

  private void LastStatementHandler()
  {
    if (dialogPlayer.currentSequence.dialogActions.Count > 0)
    {
      continueSprite.enabled = false;
      showingActions = true;
      PopulateActions(dialogPlayer.currentSequence);
    }
  }

  private void StatementStartedHandler()
  {
    continueSprite.enabled = false;
  }

  private void SequenceFinishedHandler()
  {
    Close();
    OnDialogFinish?.Invoke();
    continueSprite.enabled = false;
    SetButtonsActive(false);
  }

  private void SequenceStartedHandler()
  {
    Open();
  }

  public void Open(bool immediate = false)
  {
    canvasGroup.DOFade(1, immediate ? 0.0f : openAnimationDuration);
    rectTransform.DOScale(1, immediate ? 0.0f : openAnimationDuration);
  }

  public void Close(bool immediate = false)
  {
    canvasGroup.DOFade(0, immediate ? 0.0f : openAnimationDuration);
    rectTransform.DOScale(0, immediate ? 0.0f : openAnimationDuration);
  }

  public void PlayDialogSequence(DialogSequence sequence)
  {
    dialogPlayer.PlayDialogSequence(sequence);
  }

  private void StopPlaying(bool closeDialog = true)
  {
    if (closeDialog)
    {
      Close();
    }

    continueSprite.enabled = false;
    SetButtonsActive(false);
  }

  private void SetButtonsActive(bool active)
  {
    foreach (Button button in responseButtons)
    {
      button.gameObject.SetActive(active);
    }
  }

  private void PopulateActions(DialogSequence sequence)
  {
    for (int i = 0; i < sequence.dialogActions.Count; i++)
    {
      DialogSequence sequToTrigger = sequence.dialogActions[i].sequenceToTrigger;
      DialogSequence.DialogAction currentAction = sequence.dialogActions[i];
      responseButtons[i].gameObject.SetActive(true);
      responseButtons[i].onClick.RemoveAllListeners();
      responseButtons[i].onClick.AddListener(() =>
      {
        StopPlaying(false);
        showingActions = false;
        if (sequToTrigger != null)
          PlayDialogSequence(sequToTrigger);
        else
          dialogPlayer.Advance();
      });

      responseButtons[i].GetComponentInChildren<TextMeshProUGUI>().text = currentAction.actionName;
    }
  }
}