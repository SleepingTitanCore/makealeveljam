﻿using System.Collections.Generic;
using UnityEngine;

// Class that defines data for a sequence of dialog.
[CreateAssetMenu(fileName = "DialogSequence", menuName = "ScriptableObjects/DialogSequence", order = 1)]
[System.Serializable]
public class DialogSequence : ScriptableObject
{
  [System.Serializable]
  public class Dialog
  {
    public string speakerName;

    [TextArea]
    public string statement;
  }

  [System.Serializable]
  public class DialogAction
  {
    public string actionName;
    public DialogSequence sequenceToTrigger;
  }

  public List<Dialog> statements;
  public List<DialogAction> dialogActions;
}
